import React from 'react'
import {
  makeStyles,
  Box,
  Grid,
  Paper,
  TextField,
  Typography,
  Button,
  Container,
  InputAdornment,
  Divider,
  Hidden
} from '@material-ui/core'
import {
  AccountCircleOutlined,
  LockOutlined
} from '@material-ui/icons'

import Slider from 'react-slick'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: props => {
      if (props.admin) {
        return `url(${require('../static/admin-bg_1.jpg')})`
      }
      return `url(${require('../static/bg.jpg')})`
    },
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    left: 0,
    right: 0,
    position: 'fixed',
    height: '100%',
    overflowY: 'auto',
    overflowX: 'hidden'
  },
  paper: {
    padding: theme.spacing(5, 7),
    minHeight: 502
  },
  primaryTitle: {
    color: '#424242',
    fontWeight: theme.typography.fontWeightBold
  },
  secondaryTitle: {
    color: '#757575',
    display: 'block',
    width: '100%',
    marginBottom: 20
  },
  icons: {
    color: '#bdbdbd'
  },
  btnPrimary: {
    color: theme.palette.common.white,
    margin: theme.spacing(2, 0),
    paddingTop: 10,
    paddingBottom: 10
  },
  btnSecondary: {
    color: theme.palette.common.white,
    background: '#1de9b6',
    margin: theme.spacing(2, 0),
    paddingTop: 10,
    paddingBottom: 10,
    '&:hover': {
      background: '#00bfa5'
    }
  },
  banner: {
    width: '100%'
  },
  bannerImg: {
    display: 'block',
    width: 100,
    height: 100,
    margin: '10px auto'
  },
  divider: {
    marginTop: theme.spacing(3)
  },
  quoteWr: {
    margin: theme.spacing(4, 0)
  }
}))

const setting = {
  accessibility: false,
  dots: false,
  autoplay: true,
  arrows: false,
  fade: true
}

export const LoginComponent = (props) => {
  const classes = useStyles(props)

  return (
    <Box display="flex" flexDirection="column" justifyContent="center" className={classes.root}>
      <Container fixed>
        <Grid container spacing={4}>
          <Grid item sm={12} md={ 7 } style={{ margin: props.admin ? '0 auto' : 0 }}>
            <Paper elevation={8} className={classes.paper}>
              {
                !props.admin ? (
                  <React.Fragment>
                    <Typography variant="h6" component="h6" align="center" gutterBottom className={classes.primaryTitle}>
                      College Login
                    </Typography>
                    <Typography variant="caption" component="caption" align="center" gutterBottom className={classes.secondaryTitle}>
                      Secure Portal
                    </Typography>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                  <div className={classes.banner}>
                    <img src={require('../static/vu_logo.gif')} alt="VU logo" className={classes.bannerImg} />
                    <Typography variant="h6" component="h6" align="center" gutterBottom className={classes.primaryTitle}>
                      Vidyasagar University
                    </Typography>
                  </div>
                  <Typography variant="caption" component="caption" align="center" gutterBottom className={classes.secondaryTitle}>
                    Admin Login
                  </Typography>
                  </React.Fragment>
                )
              }
              <TextField
                label="Email / Username"
                margin="normal"
                variant="outlined"
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <AccountCircleOutlined className={classes.icons} />
                    </InputAdornment>
                  )
                }}
              />
              <TextField
                label="Password"
                margin="normal"
                variant="outlined"
                fullWidth
                type="password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <LockOutlined className={classes.icons} />
                    </InputAdornment>
                  )
                }}
              />
              <Button color={props.admin ? 'secondary' : 'primary'} variant="contained" fullWidth className={classes.btnPrimary}>
                Sign In
              </Button>
              {
                !props.admin ? (
                  <Button variant="contained" fullWidth className={classes.btnSecondary}>
                    Forgot password?
                  </Button>
                ) : null
              }
            </Paper>
          </Grid>
          {
            !props.admin ? (
              <Hidden mdDown>
                <Grid item md={5}>
                  <Paper elevation={8} className={classes.paper}>
                    <div className={classes.banner}>
                      <img src={require('../static/vu_logo.gif')} alt="VU logo" className={classes.bannerImg} />
                      <Typography variant="h6" component="h6" align="center" gutterBottom className={classes.primaryTitle}>
                        Vidyasagar University
                      </Typography>
                    </div>
                    <Divider className={classes.divider} />
                    <Slider {...setting} className={classes.quoteWr}>
                      <div>
                        <Typography variant="subtitle2" align="center" paragraph="true">
                          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Culpa quia saepe deleniti, quae magnam autem non. Dolorem provident ab possimus ex maiores.
                        </Typography>
                        <Typography variant="caption" align="right" paragraph="true">
                          - Rabindranath Tagore
                        </Typography>
                      </div>
                      <div>
                        <Typography variant="subtitle2" align="center" paragraph="true">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem quo quia vitae recusandae magni in voluptatum similique optio tempore. Reprehenderit, sint ullam?
                        </Typography>
                        <Typography variant="caption" align="right" paragraph="true">
                          - Ishwar Chandra Vidyasagar
                        </Typography>
                      </div>
                      <div>
                        <Typography variant="subtitle2" align="center" paragraph="true">
                          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem quo quia vitae recusandae magni in voluptatum similique optio tempore. Reprehenderit, sint ullam?
                        </Typography>
                        <Typography variant="caption" align="right" paragraph="true">
                          - Raja Ram Mohan Roy
                        </Typography>
                      </div>
                    </Slider>
                  </Paper>
                </Grid>
              </Hidden>
            ): null
          }
        </Grid>
      </Container>
    </Box>
  )
}

