import React from 'react'
import {
  Grid,
  Paper,
  Table,
  TableCell,
  TableBody,
  TableRow,
  TableHead,
  Button,
  InputBase
} from '@material-ui/core'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import { green, purple } from '@material-ui/core/colors'


const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
    textTransform: 'uppercase'
  },
}))(TableCell)

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow)

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2, 1)
  },
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  }
}))

function createData(
  stream,
  course,
  subject,
  option,
  collegeStatus,
  univStatus,
  student,
  enrolNum
) {
  return {
    stream,
    course,
    subject,
    option,
    collegeStatus,
    univStatus,
    student,
    enrolNum
  }
}

const rows = [
  createData(
    'arts',
    'hons',
    'bengali',
    'op1',
    'locked',
    'accepted',
    'rajat roy',
    'vu18092019'
  ),
  createData(
    'arts',
    'hons',
    'bengali',
    'op1',
    'locked',
    'accepted',
    'abhinandan sen',
    'vu18092019'
  ),
  createData(
    'science',
    'hons',
    'physics',
    'op1',
    'locked',
    'accepted',
    'ria das',
    'vu18092019'
  ),
  createData(
    'commerce',
    'hons',
    'bengali',
    'op1',
    'locked',
    'accepted',
    'priyanka roy',
    'vu18092019'
  ),
  createData(
    'science',
    'hons',
    'chemistry',
    'op1',
    'locked',
    'accepted',
    'samrat roy',
    'vu18092019'
  ),
]

const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: purple[500],
    '&:hover': {
      backgroundColor: purple[700],
    },
  },
}))(Button)
const ColorButton2 = withStyles(theme => ({
  root: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
}))(Button)

export default () => {
  const classes = useStyles()

  return (
    <React.Fragment>
      <Paper className={classes.paper}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <InputBase
            placeholder="Search by stream"
            inputProps={{ 'aria-label': 'search by stream' }}
          />
          <InputBase
            placeholder="Search by course"
            inputProps={{ 'aria-label': 'search by course' }}
          />
          <InputBase
            placeholder="Search by subject"
            inputProps={{ 'aria-label': 'search by subject' }}
          />
          <InputBase
            placeholder="Search by student name"
            inputProps={{ 'aria-label': 'search by student name' }}
          />
          <InputBase
            placeholder="Search by Enroll. number"
            inputProps={{ 'aria-label': 'search by Enroll. number' }}
          />
        </Grid>
      </Paper>
      <Paper className={classes.root}>
        <Table className={classes.table} aria-label="college dashboard">
          <TableHead>
            <TableRow>
              <StyledTableCell>Stream</StyledTableCell>
              <StyledTableCell align="right">Course</StyledTableCell>
              <StyledTableCell align="center">Subject</StyledTableCell>
              <StyledTableCell align="center">Option</StyledTableCell>
              <StyledTableCell align="center">College Status</StyledTableCell>
              <StyledTableCell align="center">University Status</StyledTableCell>
              <StyledTableCell align="center">Student Name</StyledTableCell>
              <StyledTableCell align="center">Enrollment Number</StyledTableCell>
              <StyledTableCell align="center">Actions</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <StyledTableRow key={row.name}>
                <StyledTableCell component="th" scope="row">
                  {row.stream}
                </StyledTableCell>
                <StyledTableCell align="right">{row.course}</StyledTableCell>
                <StyledTableCell align="center">{row.subject}</StyledTableCell>
                <StyledTableCell align="center">{row.option}</StyledTableCell>
                <StyledTableCell align="center">{row.collegeStatus}</StyledTableCell>
                <StyledTableCell align="center">{row.univStatus}</StyledTableCell>
                <StyledTableCell align="center">{row.student}</StyledTableCell>
                <StyledTableCell align="center">{row.enrolNum}</StyledTableCell>
                <StyledTableCell align="center">Action</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
      <Grid
        container
        direction="row"
        smDirection="column"
        justify="space-evenly"
        alignItems="center"
      >
        <ColorButton variant="contained">
          Print List
        </ColorButton>
        <ColorButton2 variant="contained" color="primary">
          Print Form A
        </ColorButton2>
        <Button variant="contained" color="secondary">
          Print Form B
        </Button>
      </Grid>
    </React.Fragment>
  )
}
