import React from 'react'
import {
  Grid,
  Paper,
  Typography,
  Table,
  TableCell,
  TableBody,
  TableRow,
  TableHead
} from '@material-ui/core'

import { withStyles, makeStyles } from '@material-ui/core/styles'

import {
  LibraryBooks,
  AccountBalance,
  AddAlarm,
  DoneAll
} from '@material-ui/icons'

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
    textTransform: 'uppercase'
  },
}))(TableCell)

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow)

function createData(
  stream,
  course,
  subject,
  total,
  opt1,
  opt2,
  collegeLock,
  collegeUnclock,
  collegeCancel,
  univAccept,
  univPending,
  univCancel
) {
  return {
    stream,
    course,
    subject,
    total,
    opt1,
    opt2,
    collegeLock,
    collegeUnclock,
    collegeCancel,
    univAccept,
    univPending,
    univCancel
  }
}

const rows = [
  createData(
    'arts',
    'hons',
    'bengali',
    350,
    300,
    250,
    30,
    50,
    60,
    23,
    12,
    30
  ),
  createData(
    'science',
    'hons',
    'bengali',
    350,
    300,
    250,
    30,
    50,
    60,
    23,
    12,
    30
  ),
  createData(
    'commerce',
    'hons',
    'bengali',
    350,
    300,
    250,
    30,
    50,
    60,
    23,
    12,
    30
  ),
  createData(
    'commerce',
    'hons',
    'bengali',
    350,
    300,
    250,
    30,
    50,
    60,
    23,
    12,
    30
  ),
  createData(
    'commerce',
    'hons',
    'bengali',
    350,
    300,
    250,
    30,
    50,
    60,
    23,
    12,
    30
  )
]

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(3, 7)
  },
  infoIcon: {
    fontSize: 50
  },
  infoCount: {
    fontWeight: theme.typography.fontWeightBold,
    color: '#757575'
  },
  infoSub: {
    color: '#bdbdbd',
    marginTop: theme.spacing(2)
  },
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  }
}))

export default () => {
  const classes = useStyles()

  return (
    <React.Fragment>
      <Grid container spacing={4}>
        <Grid item xs={12} sm={12} md={3}>
          <Paper elevation={4} className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
            >
              <AccountBalance
                className={classes.infoIcon}
                style={{color: '#633471'}}
              />
              <Typography
                variant="h4"
                component="h4"
                align="center"
                className={classes.infoCount}
              >
                18
              </Typography>
            </Grid>
            <Typography
              variant="caption"
              align="center"
              paragraph
              className={classes.infoSub}
            >
              Total number of streams
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3}>
          <Paper elevation={4} className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
            >
              <LibraryBooks
                className={classes.infoIcon}
                style={{color: '#ECA72C'}}
              />
              <Typography
                variant="h4"
                component="h4"
                align="center"
                className={classes.infoCount}
              >
                58
              </Typography>
            </Grid>
            <Typography
              variant="caption"
              align="center"
              paragraph
              className={classes.infoSub}
            >
              Total number of subjects
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3}>
          <Paper elevation={4} className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
            >
              <AddAlarm
                className={classes.infoIcon}
                style={{color: '#72b47a'}}
              />
              <Typography
                variant="h4"
                component="h4"
                align="center"
                className={classes.infoCount}
              >
                122
              </Typography>
            </Grid>
            <Typography
              variant="caption"
              align="center"
              paragraph
              className={classes.infoSub}
            >
              Total pending requests
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={3}>
          <Paper elevation={4} className={classes.paper}>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
            >
              <DoneAll
                className={classes.infoIcon}
                style={{color: '#9C89B8'}}
              />
              <Typography
                variant="h4"
                component="h4"
                align="center"
                className={classes.infoCount}
              >
                374
              </Typography>
            </Grid>
            <Typography
              variant="caption"
              align="center"
              paragraph
              className={classes.infoSub}
            >
              Total accepted requests
            </Typography>
          </Paper>
        </Grid>
      </Grid>
      <Paper className={classes.root}>
        <Table className={classes.table} aria-label="college dashboard">
          <TableHead>
            <TableRow>
              <StyledTableCell>Stream</StyledTableCell>
              <StyledTableCell align="right">Course</StyledTableCell>
              <StyledTableCell align="right">Subject</StyledTableCell>
              <StyledTableCell align="right">Total</StyledTableCell>
              <StyledTableCell align="center">Option 1</StyledTableCell>
              <StyledTableCell align="center">Option 2</StyledTableCell>
              <StyledTableCell align="center">College Lock</StyledTableCell>
              <StyledTableCell align="center">College Unlock</StyledTableCell>
              <StyledTableCell align="center">College Cancel</StyledTableCell>
              <StyledTableCell align="center">Univ. Accept</StyledTableCell>
              <StyledTableCell align="center">Univ. Pending</StyledTableCell>
              <StyledTableCell align="center">Univ. Cancel</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <StyledTableRow key={row.name}>
                <StyledTableCell component="th" scope="row">
                  {row.stream}
                </StyledTableCell>
                <StyledTableCell align="right">{row.course}</StyledTableCell>
                <StyledTableCell align="right">{row.subject}</StyledTableCell>
                <StyledTableCell align="right">{row.total}</StyledTableCell>
                <StyledTableCell align="center">{row.opt1}</StyledTableCell>
                <StyledTableCell align="center">{row.opt2}</StyledTableCell>
                <StyledTableCell align="center">{row.collegeLock}</StyledTableCell>
                <StyledTableCell align="center">{row.collegeUnclock}</StyledTableCell>
                <StyledTableCell align="center">{row.collegeCancel}</StyledTableCell>
                <StyledTableCell align="center">{row.univAccept}</StyledTableCell>
                <StyledTableCell align="center">{row.univPending}</StyledTableCell>
                <StyledTableCell align="center">{row.univCancel}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </React.Fragment>
  )
};

