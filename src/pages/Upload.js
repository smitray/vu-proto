import React, { useCallback, useMemo } from 'react'
import {
  Grid,
  Paper,
  Table,
  TableCell,
  TableBody,
  TableRow,
  TableHead,
  Button,
  Typography,
  Divider,
  Link
} from '@material-ui/core'
import { useDropzone } from 'react-dropzone'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import { purple } from '@material-ui/core/colors'
import { Badge } from 'react-bootstrap';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(3)
  },
  paperMinHeight: {
    minHeight: 268
  },
  paperTable: {
    width: '100%',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  }
}))

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
    textTransform: 'uppercase'
  },
}))(TableCell)

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow)

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '80px 20px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out'
}

const activeStyle = {
  borderColor: '#2196f3'
}

const rejectStyle = {
  borderColor: '#ff1744'
}

const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: purple[500],
    '&:hover': {
      backgroundColor: purple[700],
    },
  },
}))(Button)

function createData(
  line,
  comment,
  type
) {
  return {
    line,
    comment,
    type
  }
}

const rows = [
  createData(
    12,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'warning'
  ),
  createData(
    21,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'danger'
  ),
  createData(
    27,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'warning'
  ),
  createData(
    30,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'danger'
  ),
  createData(
    54,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'warning'
  ),
  createData(
    121,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'warning'
  ),
  createData(
    138,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'danger'
  ),
  createData(
    144,
    'Posuere convallis vestibulum vestibulum a accumsan condimentum parturient vitae vitae.',
    'warning'
  )
]

export default () => {
  const {
    paper,
    paperMinHeight,
    paperTable,
    table
  } = useStyles()

  const maxSize = 104857600

  const onDrop = useCallback(acceptedFiles => {
    console.log(acceptedFiles);
  }, [])

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    acceptedFiles,
    rejectedFiles
  } = useDropzone({
    onDrop,
    accept: 'text/csv',
    minSize: 0,
    maxSize,
  })

  const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;
  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isFileTooLarge ? rejectStyle : {})
  }), [isDragActive, isFileTooLarge])

  
  return (
    <React.Fragment>
      <Grid
        container
        spacing={4}
      >
        <Grid item sm={12} md={7}>
          <Paper elevation={4} className={paper}>
          <div { ...getRootProps({style}) }>
            <input { ...getInputProps() } />
            <p>Drag 'n' drop some files here, or click to select files</p>
            <em>(Only *.csv files will be accepted)</em>
          </div>
          </Paper>
        </Grid>
        <Grid item sm={12} md={5}>
          <Paper elevation={4} className={`${paper} ${paperMinHeight}`}>
            <Typography variant="h6" component="h6" style={{ color: '#424242' }}>
              File details
            </Typography>
            {
              acceptedFiles.length > 0 ?
                (
                  <React.Fragment>
                    {acceptedFiles.map(item => (
                      <div className="alert alert-success mt-2">
                        { item.path } - { item.size } bytes
                      </div>
                    ))}
                    <ColorButton disabled={!acceptedFiles.length > 0}>
                      Upload file
                    </ColorButton>
                  </React.Fragment>
                ) :
                (
                  <React.Fragment>
                    <div className="alert alert-warning mt-2">
                      Zero files added
                    </div>
                    <Button variant="contained" color="secondary" disabled>
                      Upload file
                    </Button>
                  </React.Fragment>
                )
            }
            <Divider style={{margin: '20px 0px 25px'}} />
            <Link
              href="http://college.vidyasagar.ac.in/Files/Data_Format.pdf?t=20190801"
              target="_blank"
              rel="noopener"
            >
              Data Capturing Format with Instruction Sheet 2019-20
            </Link>
          </Paper>
        </Grid>
      </Grid>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="center"
      >
        <Paper className={paperTable} elevation={4}>
          <Table className={table} aria-label="Error table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Line Number</StyledTableCell>
                <StyledTableCell align="center">Error</StyledTableCell>
                <StyledTableCell align="right">Type</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                rows.map(row => (
                  <StyledTableRow key={row.line}>
                    <StyledTableCell component="th" scope="row">
                      {row.line}
                    </StyledTableCell>
                    <StyledTableCell align="center">{row.comment}</StyledTableCell>
                    <StyledTableCell align="right">
                      <Badge variant={row.type}>{row.type}</Badge>
                    </StyledTableCell>
                  </StyledTableRow>
                ))
              }
            </TableBody>
          </Table>
        </Paper>
      </Grid>
    </React.Fragment>
  )
}
