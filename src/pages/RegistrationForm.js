import React, { Fragment, useState } from 'react'
import {
  makeStyles,
  Grid,
  Paper,
  Typography,
  Stepper,
  Step,
  StepLabel,
  TextField,
  FormControl,
  FormLabel,
  FormControlLabel,
  Radio,
  RadioGroup,
  Button
} from '@material-ui/core'

import {
  AccountCircle,
  SchoolRounded,
  ImportContactsRounded,
  TransferWithinAStationRounded
} from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  primaryTitle: {
    color: '#424242',
    fontWeight: theme.typography.fontWeightBold
  },
  paper: {
    padding: theme.spacing(5, 7),
    minHeight: 502
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  formspace: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2)
    }
  },
  topBio: {
    width: '100%',
    textAlign: 'center'
  }
}));

function getSteps() {
  return [
    'Personal Details',
    'Educational Details',
    'Course Details',
    'Migration Details'
  ]
}

export default () => {
  const classes = useStyles()
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <Grid item sm={12} md={7}>
            <div className={classes.topBio}>
              <AccountCircle
                style={{ fontSize: 90 }}
                color= 'primary'
              />
              <Typography variant="h5" gutterBottom>
                Personal details
              </Typography>
            </div>
            <TextField
              label="NAME OF THE STUDENT"
              fullWidth
            />
            <TextField
              label="DATE OF BIRTH"
              fullWidth
            />
            <FormControl component="fieldset">
              <FormLabel component="legend">Category</FormLabel>
              <RadioGroup aria-label="category" name="category" row>
                <FormControlLabel
                  value="general"
                  control={<Radio color="primary" />}
                  label="General"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="sc"
                  control={<Radio color="primary" />}
                  label="SC"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="st"
                  control={<Radio color="primary" />}
                  label="ST"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="obc"
                  control={<Radio color="primary" />}
                  label="OBC"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="minority"
                  control={<Radio color="primary" />}
                  label="Minority"
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
            <FormControl component="fieldset">
              <FormLabel component="legend">Gender</FormLabel>
              <RadioGroup aria-label="gender" name="gender" row>
                <FormControlLabel
                  value="male"
                  control={<Radio color="primary" />}
                  label="Male"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="female"
                  control={<Radio color="primary" />}
                  label="Female"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="others"
                  control={<Radio color="primary" />}
                  label="Others"
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
            <Grid container spacing={4}>
              <Grid item sm={12} md={6}>
                <TextField
                  label="NAME OF FATHER"
                  fullWidth
                />
              </Grid>
              <Grid item sm={12} md={6}>
                <TextField
                  label="NAME OF MOTHER"
                  fullWidth
                />
              </Grid>
            </Grid>
            <TextField
              label="PERMANENT ADDRESS"
              fullWidth
              multiline={true}
            />
            <Grid container spacing={4}>
              <Grid item sm={12} md={6}>
                <TextField
                  label="MOBILE NUMBER"
                  fullWidth
                />
              </Grid>
              <Grid item sm={12} md={6}>
                <TextField
                  label="EMAIL ADDRESS"
                  fullWidth
                />
              </Grid>
            </Grid>
            <Button variant="contained" color="primary" onClick={handleNext}>Next</Button>
          </Grid>
        );
      case 1:
        return (
          <Grid sm={12} md={7}>
            <div className={classes.topBio}>
              <SchoolRounded
                style={{ fontSize: 90 }}
                color= 'primary'
              />
              <Typography variant="h5" gutterBottom>
                Education details
              </Typography>
            </div>
            <TextField
              label="NAME OF THE COURSE"
              fullWidth
            />
            <TextField
              label="COLLEGE NAME"
              fullWidth
            />
            <TextField
              label="COLLEGE CODE"
              fullWidth
            />
            <TextField
              label="DATE OF ADMISSION"
              fullWidth
            />
            <TextField
              label="SESSION"
              fullWidth
            />
            <Button variant="contained" color="primary" onClick={handleNext}>Next</Button>
          </Grid>
        );
      case 2:
        return (
          <Grid item sm={12} md={7}>
            <div className={classes.topBio}>
              <ImportContactsRounded
                style={{ fontSize: 90 }}
                color= 'primary'
              />
              <Typography variant="h5" gutterBottom>
                Subject details
              </Typography>
            </div>
            <TextField
              label="HOUNORS / MAJOR SUBJECT"
              fullWidth
            />
            <TextField
              label="GENERAL SUBJECT"
              fullWidth
            />
            <TextField
              label="GENERAL SUBJECT"
              fullWidth
            />
            <TextField
              label="GENERAL SUBJECT"
              fullWidth
            />
            <TextField
              label="MODERN INDIAN LANGUAGE: BENGALI, HINDI"
              fullWidth
            />
            <TextField
              label="COMPULSORY ENGLISH"
              fullWidth
            />
            <FormControl component="fieldset">
              <FormLabel component="legend">Name of the stream</FormLabel>
              <RadioGroup aria-label="gender" name="gender" row>
                <FormControlLabel
                  value="bahons"
                  control={<Radio color="primary" />}
                  label="B.A.Hons"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bschons"
                  control={<Radio color="primary" />}
                  label="B.Sc.Hons"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bcomhons"
                  control={<Radio color="primary" />}
                  label="B.Com.Hons"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bamajor"
                  control={<Radio color="primary" />}
                  label="B.A.Major"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bscmajor"
                  control={<Radio color="primary" />}
                  label="B.Sc.Major"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bcommajor"
                  control={<Radio color="primary" />}
                  label="B.Com.Major"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bagen"
                  control={<Radio color="primary" />}
                  label="B.A.Gen"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bscgen"
                  control={<Radio color="primary" />}
                  label="B.Sc.Gen"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bcomgen"
                  control={<Radio color="primary" />}
                  label="B.Com.Gen"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bed"
                  control={<Radio color="primary" />}
                  label="B.Ed"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bped"
                  control={<Radio color="primary" />}
                  label="B.P.Ed"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="llb5"
                  control={<Radio color="primary" />}
                  label="LL.B - 5 years"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bvoc"
                  control={<Radio color="primary" />}
                  label="B.Voc"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bsw"
                  control={<Radio color="primary" />}
                  label="B.S.W"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="llb3"
                  control={<Radio color="primary" />}
                  label="LL.B - 3 years"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="paramedical"
                  control={<Radio color="primary" />}
                  label="Paramedical"
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="bca"
                  control={<Radio color="primary" />}
                  label="B.C.A"
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
            <Button variant="contained" color="primary" onClick={handleNext}>Next</Button>
          </Grid>
        );
      case 3:
        return (
          <Grid sm={12} md={7}>
            <div className={classes.topBio}>
              <TransferWithinAStationRounded
                style={{ fontSize: 90 }}
                color= 'primary'
              />
              <Typography variant="h5" gutterBottom>
                Migration details
              </Typography>
            </div>
            <TextField
              label="NATIONALITY"
              fullWidth
            />
            <FormControl component="fieldset">
              <FormLabel component="legend">Details of H. S. Examination passed</FormLabel>
              <TextField
                label="a) Name of the Board/ Council"
                fullWidth
              />
              <TextField
                label="b) Year of passing and Division"
                fullWidth
              />
              <TextField
                label="c) Roll No."
                fullWidth
              />
            </FormControl>
            <FormControl component="fieldset">
              <FormLabel component="legend">Details of admission to the Undergraduate Course of the Vidyasagar University</FormLabel>
              <TextField
                label="a) Name of the course"
                fullWidth
              />
              <TextField
                label="b) Session"
                fullWidth
              />
              <TextField
                label="c) Name of the College"
                fullWidth
              />
              <TextField
                label="d) Date of admission"
                fullWidth
              />
            </FormControl>
            <TextField
              label="Details of admission to any other courses of: Vidyasagar University/ Other University"
              fullWidth
            />
            <FormControl component="fieldset">
              <FormLabel component="legend">Are you a migrating student? If yes, please mention:</FormLabel>
              <TextField
                label="a) Name of University/ Institution last attended"
                fullWidth
              />
              <TextField
                label="b) Registration No. of that University with year"
                fullWidth
              />
              <TextField
                label="c) Last Examination passed with Roll No. & year"
                fullWidth
              />
              <TextField
                label="d) Migration Certificate No. with date of issue"
                fullWidth
              />
            </FormControl>
            <TextField
              label="Details of fees deposited at Cash Counter with Challan (copy enclosed)"
              fullWidth
            />
            <Button variant="contained" color="primary">Submit</Button>
          </Grid>
        );
      default:
        return 'Unknown step';
    }
  };

  return (
    <Fragment>
      <Paper className={classes.paper}>
        <Typography variant="h6" component="h6" align="center" gutterBottom className={classes.primaryTitle}>
          Registration Form
        </Typography>
        <Stepper activeStep={activeStep}>
          {steps.map((label) => {
            const stepProps = {};
            const labelProps = {};
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          className={classes.formspace}
        >
          {activeStep === steps.length ? (
            <Typography className={classes.instructions}>
              Please wait we are processing 
            </Typography>
          ) : getStepContent(activeStep)}
        </Grid>
      </Paper>
    </Fragment>
  )
}
