import React, { useState } from 'react'
import clsx from 'clsx'
import {
  AppBar,
  Toolbar,
  Typography,
  InputBase,
  IconButton,
  Badge,
  Container,
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Menu,
  Hidden
} from '@material-ui/core'

import {
  Link
} from 'react-router-dom'

import {
  makeStyles,
  fade
} from '@material-ui/core/styles'

import {
  Search,
  Mail,
  Notifications,
  AccountCircle,
  Menu as MenuIcon,
  ChevronLeft,
  Dashboard,
  FindInPage,
  AddToPhotos,
  Warning,
  Error,
  Image,
  Person,
  Settings,
  ExitToApp
} from '@material-ui/icons';


const drawerWidth = 240

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  toolBar: {
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(0)
    }
  },
  menuButton: {
    marginLeft: 12
  },
  hide: {
    display: 'none'
  },
  logoWr: {
    color: theme.palette.common.white,
    padding: '10px 32px 10px 12px'
  },
  bannerImg: {
    display: 'inline-block',
    float: 'left',
    width: 40,
    height: 'auto',
    marginRight: 15
  },
  searchWr: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    background: fade(theme.palette.common.white, 0.2),
    padding: '0 5px',
    '&:hover': {
      background: fade(theme.palette.common.white, 0.3)
    }
  },
  searchIcon: {
    position: 'absolute',
    width: theme.spacing(7),
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    color: theme.palette.common.white
  },
  inputRoot: {
    color: theme.palette.common.white
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    width: 200
  },
  iconWr: {
    padding: theme.spacing(0, 4)
  },
  contentWr: {
    marginTop: 80,
    position: 'relative',
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  listIcon: {
    color: theme.palette.primary.main
  }
}))

export const VerticalLayout = props => {
  const {
    grow,
    appBar,
    appBarShift,
    toolBar,
    logoWr,
    searchWr,
    searchIcon,
    inputRoot,
    inputInput,
    iconWr,
    bannerImg,
    contentWr,
    contentShift,
    menuButton,
    drawer,
    drawerPaper,
    drawerHeader,
    hide,
    listIcon   
  } = useStyles()

  const [open, setOpen] = useState(false)

  const [notificationEl, setNotificationEl] = useState(null)
  const [messageEl, setMessageEl] = useState(null)
  const [profileEl, setProfileEl] = useState(null)

  const isNotificationOpen = Boolean(notificationEl)
  const isMessageOpen = Boolean(messageEl)
  const isProfileOpen = Boolean(profileEl)

  const handleNotificationOpen = event => {
    setNotificationEl(event.currentTarget)
  }
  const handleNotificationClose = () => {
    setNotificationEl(null)
  }

  const handleMessageOpen = event => {
    setMessageEl(event.currentTarget)
  }
  const handleMessageClose = () => {
    setMessageEl(null)
  }

  const handleProfileOpen = event => {
    setProfileEl(event.currentTarget)
  }
  const handleProfileClose = () => {
    setProfileEl(null)
  }

  const handleDrawerOpen = () => {
    setOpen(true);
  }

  const handleDrawerClose = () => {
    setOpen(false);
  }

  const renderNotificationMenu = (
    <Menu
      anchorEl={notificationEl}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      id="notification-dd"
      keepMounted
      transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      open={isNotificationOpen}
      onClose={handleNotificationClose}
    >
      <List component="nav">
        <ListItem>
          <ListItemIcon className="text-warning">
            <Warning />
          </ListItemIcon>
          <ListItemText primary="Warning notification 123" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon className="text-danger">
            <Error />
          </ListItemIcon>
          <ListItemText primary="Warning notification 123" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon className="text-warning">
            <Warning />
          </ListItemIcon>
          <ListItemText primary="Warning notification 123" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon className="text-warning">
            <Warning />
          </ListItemIcon>
          <ListItemText primary="Warning notification 123" />
        </ListItem>
      </List>
    </Menu>
  )

  const renderMessageMenu = (
    <Menu
      anchorEl={messageEl}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      id="message-dd"
      keepMounted
      transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      open={isMessageOpen}
      onClose={handleMessageClose}
    >
      <List component="nav">
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <Image />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Abhinandan B" secondary="Jan 9, 2014 - 09:46 AM" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <Image />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Abhinandan B" secondary="Jan 9, 2014 - 09:46 AM" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <Image />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Abhinandan B" secondary="Jan 9, 2014 - 09:46 AM" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <Image />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Abhinandan B" secondary="Jan 9, 2014 - 09:46 AM" />
        </ListItem>
      </List>
    </Menu>
  )

  const renderProfileMenu = (
    <Menu
      anchorEl={profileEl}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      id="profile-dd"
      keepMounted
      transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      open={isProfileOpen}
      onClose={handleProfileClose}
    >
      <List component="nav">
        <ListItem>
          <ListItemIcon>
            <Person />
          </ListItemIcon>
          <ListItemText primary="Profile" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <Settings />
          </ListItemIcon>
          <ListItemText primary="Profile Settings" />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText primary="Sign Out" />
        </ListItem>
      </List>
    </Menu>
  )

  return (
    <div className={grow} style={{'position': 'relative'}}>
      <AppBar
        position="fixed"
        className={clsx(appBar, {
          [appBarShift]: open,
        })}
      >
        <Toolbar className={`${toolBar} d-flex justify-content-between`}>
          <div className="d-flex align-items-center">
            <IconButton
              edge="start"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={clsx(`${menuButton} text-white`, open && hide)}
            >
              <MenuIcon />
            </IconButton>
            <div className={`${logoWr} d-flex align-items-center`}>
              <img src={require('../static/vu_logo.png')} alt="VU logo" className={bannerImg} />
              <Typography variant="h6" noWrap style={{'float': 'left'}}>
                Vidyasagar University
              </Typography>
            </div>
            <Hidden mdDown>
              <div className="mb-0 mx-4 d-flex align-items-center">
                <div className={searchWr}>
                  <div className={searchIcon}>
                    <Search />
                  </div>
                  <InputBase
                    placeholder="Search…"
                    classes={{
                      root: inputRoot,
                      input: inputInput,
                    }}
                    inputProps={{ 'aria-label': 'search' }}
                  />
                </div>
              </div>
            </Hidden>
          </div>
          <Hidden mdDown>
            <div className={`${iconWr} d-flex align-items-center`}>
              <IconButton
                aria-label="show 4 new mails"
                className="text-white"
                aria-controls="message-dd"
                aria-haspopup="true"
                onClick={handleMessageOpen}
              >
                <Badge badgeContent={4} color="secondary">
                  <Mail />
                </Badge>
              </IconButton>
              <IconButton
                aria-label="show 17 new notifications"
                className="text-white"
                aria-controls="notification-dd"
                aria-haspopup="true"
                onClick={handleNotificationOpen}
              >
                <Badge badgeContent={17} color="secondary">
                  <Notifications />
                </Badge>
              </IconButton>
              <IconButton
                edge="end"
                aria-label="account of current user"
                className="text-white"
                aria-controls="profile-dd"
                aria-haspopup="true"
                onClick={handleProfileOpen}
              >
                <AccountCircle />
              </IconButton>
            </div>
          </Hidden>
        </Toolbar>
      </AppBar>
      <Drawer
        className={drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: drawerPaper,
        }}
      >
        <div className={drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeft />
          </IconButton>
        </div>
        <Divider />
        <List component="nav">
          <ListItem component={Link} to="/vertical/dashboard" button onClick={handleDrawerClose}>
            <ListItemIcon className={listIcon}>
              <Dashboard />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
          <ListItem component={Link} to="/vertical/search" button onClick={handleDrawerClose}>
            <ListItemIcon className={listIcon}>
              <FindInPage />
            </ListItemIcon>
            <ListItemText primary="Search Student" />
          </ListItem>
          <ListItem component={Link} to="/vertical/upload" button onClick={handleDrawerClose}>
            <ListItemIcon className={listIcon}>
              <AddToPhotos />
            </ListItemIcon>
            <ListItemText primary="Upload Data" />
          </ListItem>
        </List>
      </Drawer>
      <div
        className={clsx(contentWr, {
          [contentShift]: open,
        })}
      >
        <Container>
          { props.children }
        </Container>
      </div>
      {renderNotificationMenu}
      {renderMessageMenu}
      {renderProfileMenu}
    </div>
  )
}
