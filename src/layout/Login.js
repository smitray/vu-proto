import React from 'react'
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    minHeight: '100vh',
    width: '100%'
  }
}))

export const LoginLayout = props => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      { props.children }
    </div>
  )
}
