import React from 'react'
import {
  AppBar,
  Toolbar,
  Typography,
  InputBase,
  IconButton,
  Badge,
  Container,
  Hidden
} from '@material-ui/core'

import {
  Link
} from 'react-router-dom'

import {
  makeStyles,
  fade
} from '@material-ui/core/styles'

import {
  Search,
  Mail,
  Notifications,
  AccountCircle
} from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  headerWr: {
    top: 0,
    left: 'auto',
    right: 0,
    position: 'fixed',
    width: '100%',
    zIndex: 9999
  },
  appBar: {
    backgroundColor: theme.palette.primary.main,
    color: '#424242'
  },
  toolBar: {
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(0)
    }
  },
  logoWr: {
    color: theme.palette.common.white,
    padding: '10px 32px'
  },
  bannerImg: {
    display: 'inline-block',
    float: 'left',
    width: 40,
    height: 'auto',
    marginRight: 15
  },
  searchWr: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    background: fade(theme.palette.common.white, 0.2),
    padding: '0 5px',
    '&:hover': {
      background: fade(theme.palette.common.white, 0.3)
    }
  },
  searchIcon: {
    position: 'absolute',
    width: theme.spacing(7),
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    color: theme.palette.common.white
  },
  inputRoot: {
    color: theme.palette.common.white
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    width: 200
  },
  iconWr: {
    padding: theme.spacing(0, 4)
  },
  navWr: {
    background: '#FFF',
    display: 'flex',
    height: theme.spacing(6),
    zIndex: 99,
    boxShadow: '0 2px 10px 0 rgba(204, 204, 204, 0.5)'
  },
  navUl: {
    '& li': {
      '& a': {
        color: '#464D69',
        padding: '0.9375rem 1.563rem 0.9375rem 1.563rem',
        fontSize: '0.875rem',
        textTransform: 'capitalize',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 500
      }
    }
  },
  contentWr: {
    marginTop: 140,
    position: 'relative'
  }
}))

export const DefaultLayout = props => {
  const {
    grow,
    appBar,
    toolBar,
    logoWr,
    searchWr,
    searchIcon,
    inputRoot,
    inputInput,
    iconWr,
    bannerImg,
    headerWr,
    navWr,
    navUl,
    contentWr    
  } = useStyles()

  return (
    <div className={grow} style={{'position': 'relative'}}>
      <div className={headerWr}>
        <AppBar className={appBar} position="relative" elevation={0}>
          <Toolbar className={`${toolBar} d-flex justify-content-between`}>
            <div className="d-flex align-items-center">
              <div className={`${logoWr} d-flex align-items-center`}>
                <img src={require('../static/vu_logo.png')} alt="VU logo" className={bannerImg} />
                <Typography variant="h6" noWrap style={{'float': 'left'}}>
                  Vidyasagar University
                </Typography>
              </div>
              <Hidden mdDown>
                <div className="mb-0 mx-4 d-flex align-items-center">
                  <div className={searchWr}>
                    <div className={searchIcon}>
                      <Search />
                    </div>
                    <InputBase
                      placeholder="Search…"
                      classes={{
                        root: inputRoot,
                        input: inputInput,
                      }}
                      inputProps={{ 'aria-label': 'search' }}
                    />
                  </div>
                </div>
              </Hidden>
            </div>
            <Hidden mdDown>
              <div className={`${iconWr} d-flex align-items-center`}>
                <IconButton aria-label="show 4 new mails" className="text-white">
                  <Badge badgeContent={4} color="secondary">
                    <Mail />
                  </Badge>
                </IconButton>
                <IconButton aria-label="show 17 new notifications" className="text-white">
                  <Badge badgeContent={17} color="secondary">
                    <Notifications />
                  </Badge>
                </IconButton>
                <IconButton
                  edge="end"
                  aria-label="account of current user"
                  className="text-white"
                >
                  <AccountCircle />
                </IconButton>
              </div>
            </Hidden>
          </Toolbar>
        </AppBar>
        <div className={navWr}>
          <ul className={`${navUl} nav align-items-center`}>
            <li className="nav-item">
              <Link to="/dashboard" className="nav-link">
                Dashboard
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/search" className="nav-link">
                Search
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/upload" className="nav-link">
                Upload Data
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/registration" className="nav-link">
                Registration
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className={contentWr}>
        <Container>
          { props.children }
        </Container>
      </div>
    </div>
  )
}
