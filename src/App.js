import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { CssBaseline } from '@material-ui/core'
import { red } from '@material-ui/core/colors'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

import { CountainerRoute } from './layout/Container'
import { DefaultLayout } from './layout/Default'
import { LoginLayout } from './layout/Login'
import { VerticalLayout } from './layout/Vertical'

import Dashboard from './pages/Dashboard';
import Upload from './pages/Upload';
import Search from './pages/Search';
import RegistrationForm from './pages/RegistrationForm';
import {Login} from './pages/Login';
import {AdminLogin} from './pages/AdminLogin';

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: red[500]
    }
  }
})

export default () => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Router>
      <Switch>
        <CountainerRoute exact path="/" layout={ LoginLayout } component={ Login } />
        <CountainerRoute exact path="/admin" layout={ LoginLayout } component={ AdminLogin } />
        <CountainerRoute exact path="/dashboard" layout={ DefaultLayout } component={ Dashboard } />
        <CountainerRoute exact path="/upload" layout={ DefaultLayout } component={ Upload } />
        <CountainerRoute exact path="/search" layout={ DefaultLayout } component={ Search } />
        <CountainerRoute exact path="/registration" layout={ DefaultLayout } component={ RegistrationForm } />
        <CountainerRoute exact path="/vertical/dashboard" layout={ VerticalLayout } component={ Dashboard } />
        <CountainerRoute exact path="/vertical/search" layout={ VerticalLayout } component={ Search } />
        <CountainerRoute exact path="/vertical/upload" layout={ VerticalLayout } component={ Upload } />
      </Switch>
    </Router>
  </MuiThemeProvider>
)
